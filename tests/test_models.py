import json
import unittest
from typing import List

from marshmallow import fields, ValidationError

from toastedmarshmallow_models import Model, NestedModel, SelfReferencingModel


class EntityWithUnderscoreAttributeNameInConstructor(Model):
    id = fields.Integer(attribute='_id')
    name = fields.String()

    def __init__(self, id, name):
        self._id = id
        self.name = name


class DummyEntity(Model):
    id = fields.Integer()
    name = fields.String()

    def __init__(self, id, name):
        self.id = id
        self.name = name


class DummyEntityWithValidationInInit(Model):
    id = fields.Integer()
    name = fields.String()

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name
        self.validate()


class ChildEntity(Model):
    name = fields.String()

    def __init__(self, name: str):
        self.name = name


class BaseClass(Model):
    id = fields.Int()

    def __init__(self, id):
        self.id = id


class InheritedClass(BaseClass):
    value = fields.Str()

    def __init__(self, id, value):
        super().__init__(id)
        self.id = id
        self.value = value


class ParentEntity(Model):
    name = fields.String()
    children = fields.Nested(NestedModel(ChildEntity), many=True)

    def __init__(self, name: str, children: List[ChildEntity]):
        self.children = children
        self.name = name


class SelfReferencingEntity(Model):
    name = fields.String()
    children = fields.Nested(SelfReferencingModel('SelfReferencingEntity'), many=True, allow_none=True)

    def __init__(self, name: str, children: List['SelfReferencingEntity'] = None):
        self.children = children
        self.name = name


class ModelsTests(unittest.TestCase):
    def setUp(self):
        self.data = dict(id=1, name='John')
        self.wrong_data = dict(id='i-should-be-an-integer', name='John')
        self.entity = DummyEntity(**self.data)
        self.wrong_entity = DummyEntity(**self.wrong_data)

    def test_init_should_succeed_given_valid_data(self):
        # when
        result = DummyEntityWithValidationInInit(**self.data)

        # then
        self.assertEqual(1, result.id)

    def test_init_should_throw_given_invalid_data(self):
        # when & then
        with self.assertRaises(ValidationError):
            DummyEntityWithValidationInInit(**self.wrong_data)

    def test_from_dict(self):
        # when
        result = DummyEntity.from_dict(self.data)

        # then
        self.assertEqual(result.id, self.data.get('id'))
        self.assertEqual(result.name, self.data.get('name'))
        self.assertIsInstance(result, DummyEntity)

    def test_from_dict_should_throw_given_wrong_data(self):
        # when
        with self.assertRaises(ValidationError) as e:
            DummyEntity.from_dict(self.wrong_data)

        # then
        self.assertDictEqual(e.exception.kwargs, dict(klass='DummyEntity'))
        self.assertDictEqual(e.exception.messages, dict(id=['Not a valid integer.']))

    def test_from_json(self):
        # given
        json_data = json.dumps(self.data)

        # when
        result = DummyEntity.from_json(json_data)

        # then
        self.assertEqual(result.id, self.data.get('id'))
        self.assertEqual(result.name, self.data.get('name'))
        self.assertIsInstance(result, DummyEntity)

    def test_from_json_should_throw_given_wrong_data(self):
        # given
        wrong_json_data = json.dumps(self.wrong_data)

        # when
        with self.assertRaises(ValidationError) as e:
            DummyEntity.from_json(wrong_json_data)

        # then
        self.assertDictEqual(e.exception.kwargs, dict(klass='DummyEntity'))
        self.assertDictEqual(e.exception.messages, dict(id=['Not a valid integer.']))

    def test_to_dict(self):
        # when
        result = self.entity.to_dict()

        # then
        self.assertDictEqual(result, self.data)

    def test_to_dict_should_throw_given_wrong_data(self):
        # when
        with self.assertRaises(ValidationError) as e:
            self.wrong_entity.to_dict()

        # then
        self.assertDictEqual(e.exception.kwargs, dict(klass='DummyEntity'))
        self.assertDictEqual(e.exception.messages, dict(id=['Not a valid integer.']))

    def test_to_json(self):
        # given
        expected_result = json.dumps(self.data)

        # when
        result = self.entity.to_json()

        # then
        self.assertEqual(result, expected_result)

    def test_to_json_should_throw_given_wrong_data(self):
        # when
        with self.assertRaises(ValidationError) as e:
            self.wrong_entity.to_json()

        # then
        self.assertDictEqual(e.exception.kwargs, dict(klass='DummyEntity'))
        self.assertDictEqual(e.exception.messages, dict(id=['Not a valid integer.']))

    def test_validate_should_not_throw_given_valid_data(self):
        # when & then
        self.entity.validate()

    def test_validate_should_throw_given_wrong_data(self):
        # when
        with self.assertRaises(ValidationError) as e:
            self.wrong_entity.to_json()

        # then
        self.assertDictEqual(e.exception.kwargs, dict(klass='DummyEntity'))
        self.assertDictEqual(e.exception.messages, dict(id=['Not a valid integer.']))

    def test_get_validation_errors_should_return_empty_dict_given_valid_entity(self):
        # when
        result = self.entity.get_validation_errors()

        # then
        self.assertDictEqual(dict(), result)

    def test_get_validation_errors_should_return_dict_with_errors_given_invalid_entity(self):
        # when
        result = self.wrong_entity.get_validation_errors()

        # then
        self.assertDictEqual(result, dict(id=['Not a valid integer.']))

    def test_self_referencing_relationship(self):
        # given
        daughter = SelfReferencingEntity(name='Daughter')
        son = SelfReferencingEntity(name='Son')
        parent = SelfReferencingEntity(name='Mama', children=[son, daughter])
        dict_data = parent.to_dict()

        # when
        result = SelfReferencingEntity.from_dict(dict_data)

        # then
        self.assertDictEqual(
            {'name': 'Mama', 'children': [{'name': 'Son', 'children': None}, {'name': 'Daughter', 'children': None}]},
            dict_data)
        self.assertEqual(parent.name, result.name)
        self.assertEqual(2, len(result.children))
        self.assertEqual('Son', result.children[0].name)
        self.assertEqual('Daughter', result.children[1].name)

    def test_inherited_class_should_also_serialize_fields_from_parent_class(self):
        # given
        instance = InheritedClass(id=1, value='Val')
        data = instance.to_dict()

        # when
        result = InheritedClass.from_dict(data)

        # then
        self.assertEqual(instance.id, result.id)
        self.assertEqual(instance.value, result.value)

    def test_to_and_from_dict_for_entity_with_attributes_that_have_underscores_in_constructor(self):
        # given
        entity = EntityWithUnderscoreAttributeNameInConstructor(id=1, name='John')
        data = entity.to_dict()

        # when
        result = EntityWithUnderscoreAttributeNameInConstructor.from_dict(data)

        # then
        self.assertEqual(result._id, 1)
        self.assertEqual(result.name, 'John')



